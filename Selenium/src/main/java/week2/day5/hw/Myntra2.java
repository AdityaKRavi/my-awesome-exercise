package week2.day5.hw;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Myntra2 {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.myntra.com/");
		WebElement searchBox = driver.findElementByTagName("input");
		Actions builder = new Actions(driver);
		builder.click(searchBox).sendKeys("Sunglasses").sendKeys(Keys.ENTER).perform();
//		List<WebElement> allSunglasses = driver.findElementsByXPath("//li[@class='product-base']");
//		List<WebElement> productName = driver.findElementsByXPath("//h4[@class ='product-product']");
//		List<WebElement> discountPrice = driver.findElementsByXPath("//span[@class ='product-discountPercentage']");
		
//		String productName = driver.findElementByXPath("//h4[@class ='product-product']").getText();
//		String discountPrice = driver.findElementByXPath("//span[@class ='product-discountPercentage']").getText();
//		for(int i=0;i<allSunglasses.size();i++) {
//			String product = productName.get(i).getText();
//			String discount = discountPrice.get(i).getText();
//			if(product.contains("Unisex") && discount.equals("(40% OFF)")) {
//				Thread.sleep(1000);
//				String brand =driver.findElementByXPath("(//span[text()='(40% OFF)']//parent::div//preceding-sibling::div)["+i+"]").getText();
//				System.out.println(brand);
//			}
//		}
		List<WebElement> allDiscount40 = driver.findElementsByXPath("//span[text()='(40% OFF)']//parent::div//preceding-sibling::h4");
		List<WebElement> allProd = driver.findElementsByXPath("//h4[contains(text(),'Unisex')]");
		if(allProd.size() > allDiscount40.size()){
			for (WebElement webElement2 : allDiscount40) {
				for (WebElement webElement : allProd) {
					if(webElement.getText().equals(webElement2.getText()))
						System.out.println(webElement2.getText());
				}
			}
		}
	}
}
