package exercise1;

import java.text.*;
import java.util.Locale;

public class PracticeRegex {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			String a="Rs 1,20,000";
			String b=a.replaceAll("[^0-9,]", "");
			
			System.out.println(b);
//			DecimalFormat df=new DecimalFormat();
			
			Locale locale = new Locale("hin", "IN");
			NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
			String format = numberFormat.format(100000000);
			System.out.println(format);
			long num=10000;
			DecimalFormat df = new DecimalFormat("##,##,###");
			System.out.println(df.format(num));
	}
}
